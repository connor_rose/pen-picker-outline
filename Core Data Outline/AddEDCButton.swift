//
//  AddEDCButton.swift
//  Core Data Outline
//
//  Created by Connor Rose on 1/11/21.
//

import SwiftUI

struct AddEDCButton: View {
    
    @Environment(\.managedObjectContext) var moc
    @State private var showingAddEDCMenuScreen = false
    
    var body: some View {
        
        Button(action: {
            self.showingAddEDCMenuScreen.toggle()
        }) {
            ZStack {
                Circle()
                    .foregroundColor(Color.white)
                    .frame(width: 50, height: 50)
                    .shadow(radius: 4, x: 2, y: 3)
                Image(systemName: "plus.circle.fill")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 50, height: 50)
                    .foregroundColor(.orange)
            }
            
        }.sheet(isPresented: $showingAddEDCMenuScreen) {
            AddEDCMenu(carry: Carry, ink: Ink).environment(\.managedObjectContext, self.moc)
        }
        
    }



struct AddEDCButton_Previews: PreviewProvider {
    static var previews: some View {
        AddEDCButton()
    }
}






struct AddEDCMenu: View {
    @Environment(\.presentationMode) var carrypresentationMode
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(entity: Carry.entity(), sortDescriptors: []) var carries: FetchedResults<Carry>
    @ObservedObject var carry: Carry
    @ObservedObject var ink: Ink
    @State private var inked: Bool = false //convert to checkbox item
 
    @State private var edcDate = Date()
    @State private var clean: Bool = false
    
    
//    @ObservedObject var ink: Ink
    @State var isPickingProject = false
    
    var body: some View {
        NavigationView {
            Form {
                Text(carry.currentink?.name ?? "")
                NavigationLink(
                    destination:
                       InkPicker(
                            isVisible: $isPickingProject,
                        inks: Ink.getAllInks(context: moc),
                            onPick: setInk(_:),
                            cellBuilder: inkCell(for:)
                        ),
                    isActive: $isPickingProject
                ) {
                    HStack {
                        Text("Project")
                        Spacer()
                        Text(carry.currentink?.name ?? "Inbox")
                    }
                }
                
            }.navigationTitle("Today's EDC")
            .navigationBarItems(leading: Button(action: {
                self.carrypresentationMode.wrappedValue.dismiss()
                }) {
                    Text("Cancel")
                }, trailing:  Button("Save") {
                    let newCarry = Carry(context: self.moc)
//                    newCarry.pen = self.pen
//                    newCarry.ink = self.ink
//                    newCarry.currentpen = Pen(context: self.moc)
//                    newCarry.currentink = Ink(context: self.moc)
                    
                    newCarry.edcDate = self.edcDate
                    newCarry.clean = false
                    newCarry.dirty = false
                    newCarry.id = UUID()
                    try? self.moc.save()
                    self.carrypresentationMode.wrappedValue.dismiss()
                })
           
        }
        func inkCell(for ink: Ink) -> some View {
            HStack {
                Text(carry.currentink?.name ?? "")
                Spacer()
            }
        }
        
        func setInk(_ ink: Ink) {
            self.carry.currentink = ink
            try? self.moc.save()
        }
}
}
}
