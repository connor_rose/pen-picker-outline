//
//  EDCListView.swift
//  Penedex
//
//  Created by Connor Rose on 7/21/20.
//

import SwiftUI

struct EDCListView: View {
    @Environment(\.managedObjectContext) var moc
    
    @FetchRequest(entity: Carry.entity(), sortDescriptors: [NSSortDescriptor.init(key: "edcDate", ascending: false)]) var carry: FetchedResults<Carry>
    
    @FetchRequest(entity: Carry.entity(), sortDescriptors: [NSSortDescriptor.init(key: "edcDate", ascending: false)], predicate: NSPredicate(format: "clean == true")) var cleanedcarry: FetchedResults<Carry>
    
    @FetchRequest(entity: Carry.entity(), sortDescriptors: [NSSortDescriptor.init(key: "edcDate", ascending: false)], predicate: NSPredicate(format: "clean == false")) var inkedcarry: FetchedResults<Carry>
    
    @State private var showingSettingsScreen = false
    @State private var showcleantoggle = false
    var currentdate : Date = Date()
    
    static let taskDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }()
    
    let layout = [
        GridItem(.flexible())
    ]
//Consider a filter button for filtering out clean v inked EDC's
        
    @ViewBuilder
    var body: some View {
        if inkedcarry.isEmpty {
            emptyEDCView()
        } else {
        NavigationView {
            ZStack {
                GeometryReader { geometry in
                    ScrollView {
                        LazyVGrid(columns: layout, spacing: 10) {
                            
                            ForEach(inkedcarry) { carry in
                                NavigationLink(destination:
                                                EDCDetailView(carry: carry)
                                ){
                                    HStack {
//                                        ZStack {
//                                            if carry.clean == true {
//                                                Image(systemName: "wand.and.stars")
//                                                    .font(.system(size: 25))
//                                            }
//                                            else {
//                                        inkCounter()
//                                            .font(.system(size: 45))
//                                            .foregroundColor(.primary)
//                                            }
//                                        }
                                        
                                        VStack(alignment: .leading, spacing: 2) {
                                            Text("\(carry.edcDate ?? Date(), formatter: Self.taskDateFormat)")
                                                .fontWeight(.bold)
                                                .foregroundColor(.primary)
                                            
                                            Label {
                                                Text(carry.currentpen?.name ?? "No Pen")
                                                    .foregroundColor(.primary)
                                                    .lineLimit(1)
                                                    .offset(x: 4)
                                            } icon: {
                                                Image("SFNib")
                                                    .resizable()
                                                    .frame(width: 15, height: 18)
                                                    .offset(x: 2, y:2)
                                                    
                                            }
                                                .foregroundColor(.primary)
                                                .lineLimit(1)
                                            
                                                
                                            Label {
                                                Text(carry.currentink?.name ?? "No Ink")
                                                    .foregroundColor(.primary)
                                                    .lineLimit(1)
                                            } icon: {
                                               Image(systemName: "loupe")
                                                .foregroundColor(.accentColor)
                                            }
                                                .foregroundColor(.primary)
                                                .lineLimit(1)
                                            
                                        }.padding(.horizontal)
                                        Spacer()
                                        Image(systemName: carry.dirty ? "eyedropper.full" : "")
                                            .font(.system(size: 20))
                                            .padding(.trailing)
                                        
                                        Image(systemName: "chevron.right")
                                        
                                    }.frame(maxWidth: geometry.size.width * 0.8)
                                    .padding()
                                    .background(Color.gray.opacity(0.2))
                                    .cornerRadius(15)
                                }
                                
                                
                            }
                            
                        }
                        Text("Total carries: \(carry.count)") .padding()
                    }
                    
                }
                VStack {
                    Spacer()
                    HStack {
                        Spacer()
                        AddEDCButton()
                            .padding([.bottom, .trailing])
                    }
                }
                    }
            .navigationTitle("Penedex")
            .navigationBarItems(
//                leading: Button(action: {
//                    self.showcleantoggle = true
//                }) {
//                    Image(systemName: showcleantoggle ? "wand.and.rays" : "wand.and.stars.inverse")
//                },
                                trailing:
                                    Button(action: {
                                        self.showingSettingsScreen.toggle()
                                    }) {
                                        Image(systemName: "gear")
                                            .foregroundColor(.orange)
                                            .imageScale(.large)
                                    })
                }
        }
            }
            //.frame(idealWidth: 200)
            
    struct emptyEDCView: View {
        var body: some View {
            
            VStack {
                Spacer()
                ZStack {
                    RoundedRectangle(cornerRadius: 50, style: .continuous)
                                   .fill(Color.secondary)
                                   .frame(width: 150, height: 150)
                       
                Image("StubNib")
                    .resizable()
                    .frame(width: 150, height: 150)
                    .rotation3DEffect(.degrees(180), axis: (x: 0, y: 0, z: 1))
                }
                Text("Looks like you need to ink up a pen!")
                    .font(.system(.body, design: .rounded))
            
            
                Spacer()
                HStack {
                    Spacer()
                    AddEDCButton()
                        .padding([.bottom, .trailing])
                }
            
            }
        
        }
    }
}
    
    
    struct inkCounter: View {
        let timer = Timer.publish(every: 86400, tolerance: 86350, on: .main, in: .common).autoconnect()
        @State private var counter = 0
        @State private var inkTimer: Int = 0
        
        @FetchRequest(entity: Carry.entity(), sortDescriptors: []) var carry: FetchedResults<Carry>
        
        var body: some View {
           
            Text("\(inkTimer)")
                .onReceive(timer) { time in
                    self.inkTimer += 1
                }
                }
        }
    
    struct EDCContextMenu: View {
        @Environment(\.managedObjectContext) var moc
        @FetchRequest(entity: Carry.entity(), sortDescriptors: []) var carrys: FetchedResults<Carry>
       
        func deleteCarry() {
               for carry in carrys {
                   
                moc.delete(carry)
               }
               try? moc.save()
           }
        
        var body: some View {
            VStack {
            Button(action: {
                    deleteCarry()}, label: {Text("Delete"); Image(systemName: "minus")
                    })
            }
    }
    }
    

struct EDCListView_Previews: PreviewProvider {
    static var previews: some View {
        EDCListView()
    }
}
