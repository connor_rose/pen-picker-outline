//
//  penExtension.swift
//  Core Data Outline
//
//  Created by Connor Rose on 1/11/21.
//

import SwiftUI
import CoreData

extension Pen {
    static func getAllPens(context: NSManagedObjectContext) -> [Pen] {
        let request: NSFetchRequest<Pen> = Pen.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(keyPath: \Pen.name, ascending: true)]
        return (try? context.fetch(request)) ?? []
    }
}
