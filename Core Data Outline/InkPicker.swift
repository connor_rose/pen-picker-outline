import SwiftUI


struct InkPicker<T: Identifiable, Cell: View>: View {
 //   @FetchRequest(entity: Ink.entity(), sortDescriptors: []) var inks: FetchedResults<Ink>

    // used to dismiss after the user picks an item
    // you should pass the NavigationLink's isActive or sheet's isVisible boolean
    // if you don't want auto dismissal, you can easily remove this
    @Binding var isVisible: Bool
    
    // the list of items to pick from
    let inks: [T]
    
    // what happens when a pick is made
    let onPick: (T) -> Void
    
    // how to display each item
    let cellBuilder: (T) -> Cell
    
    var body: some View {
        List(inks) { ink in
            Button(action: {
                self.onPick(ink)
                self.isVisible = false
            }) {
                cellBuilder(ink)
            }
        }
    }
}
