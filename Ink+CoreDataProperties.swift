//
//  Ink+CoreDataProperties.swift
//  Core Data Outline
//
//  Created by Connor Rose on 1/5/21.
//
//

import Foundation
import CoreData


extension Ink {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Ink> {
        return NSFetchRequest<Ink>(entityName: "Ink")
    }

    @NSManaged public var color: String?
    @NSManaged public var dryTime: Int16
    @NSManaged public var flow: Int16
    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var notes: String?
    @NSManaged public var purchaseDate: Date?
    @NSManaged public var saturation: Int16
    @NSManaged public var shading: Int16
    @NSManaged public var sheen: Int16
    @NSManaged public var shimmer: Bool
    @NSManaged public var brand: Brand?
    @NSManaged public var carries: NSSet?

}

// MARK: Generated accessors for carries
extension Ink {

    @objc(addCarriesObject:)
    @NSManaged public func addToCarries(_ value: Carry)

    @objc(removeCarriesObject:)
    @NSManaged public func removeFromCarries(_ value: Carry)

    @objc(addCarries:)
    @NSManaged public func addToCarries(_ values: NSSet)

    @objc(removeCarries:)
    @NSManaged public func removeFromCarries(_ values: NSSet)

}

extension Ink : Identifiable {

}
