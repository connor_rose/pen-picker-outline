//
//  DetailsView.swift
//  Core Data Outline
//
//  Created by Connor Rose on 1/11/21.
//

import SwiftUI

struct DetailsView: View {
    @Environment(\.managedObjectContext) private var moc
    @ObservedObject var carry: Carry
    @ObservedObject var pen: Pen
    @State var isPickingProject = false
    
    var body: some View {
        Form {
            Text(carry.currentpen?.name ?? "")
            NavigationLink(
                destination:
                    PickerList(
                        isVisible: $isPickingProject,
                        pens: Pen.getAllPens(context: moc),
                        onPick: setPen(_:),
                        cellBuilder: penCell(for:)
                    ),
                isActive: $isPickingProject
            ) {
                HStack {
                    Text("Project")
                    Spacer()
                    Text(carry.currentpen?.name ?? "Inbox")
                }
            }
        }.navigationTitle(carry.currentpen?.name ?? "")
    }
    
    func penCell(for pen: Pen) -> some View {
        HStack {
            Text(carry.currentpen?.name ?? "")
            Spacer()
        }
    }
    
    func setPen(_ pen: Pen) {
        self.carry.currentpen = pen
        try? self.moc.save()
    }
}

