//
//  EDCDetailView.swift
//  iOS
//
//  Created by Connor Rose on 9/9/20.
//

import SwiftUI

struct EDCDetailView: View {
    @State private var showingAlert = false
    @Environment(\.managedObjectContext) var moc
    
    @FetchRequest(entity: Carry.entity(), sortDescriptors: []) var carrys: FetchedResults<Carry>
    
    var carry:Carry
    var currentdate : Date = Date()
    
    
    static let taskDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }()
    
    @State private var showingEditMenuScreen = false
    @Environment(\.presentationMode) var editpresentationMode
    
    
    var body: some View {
        
        GeometryReader { geometry in
            ScrollView {
                
                VStack(alignment: .leading) {
                    Text("Ink'd on \(carry.edcDate ?? Date(), formatter: Self.taskDateFormat)")
                        .font(.title)
                        .padding(.bottom)
                    
                    Text(carry.currentpen?.name ?? "No pen")
                        .font(.title)
                    
                    Text(carry.currentink?.name ?? "No ink")
                        .font(.title)
                   
                    HStack {
                        if carry.clean == true {
                    HStack {
                       
                        if carry.clean == true {
                           Image(systemName: carry.clean ? "wand.and.stars" : "")

                       } else {Text("")}

                    }
                        }
                        else {
                    HStack {
                       
                        if carry.dirty == true {
                           Image(systemName: carry.dirty ? "eyedropper.full" : "")

                       } else {Text("")}

                    }
                        }
                       // Spacer()
                    }.padding()
                    
                   
                
                    Section {
                        Text(carry.notes ?? "No Notes")
                            .multilineTextAlignment(.leading)
                            .padding(.top)
                            .foregroundColor(Color.primary)
                        
                    }
                    .padding(.horizontal)
                    .frame(width: 360, height: 100, alignment: .topLeading)
                    .background(Color.secondary.opacity(0.2))
                    .cornerRadius(10)
                    
                    HStack {
                        Spacer()
                            
                            Button(action: {
                                self.showingAlert = true; self.moc.delete(carry)
                                do {
                                    try self.moc.save()
                                }catch{
                                    print(error)
                                }
                                   }) {
                                       Text("Delete").foregroundColor(Color.red)
                                   }
                                   .alert(isPresented:$showingAlert) {
                                       Alert(title: Text("Are you sure you want to delete this?"), message: Text("This cannot be undone."), primaryButton: .destructive(Text("Delete")) {
                                              
                                       }, secondaryButton: .cancel())
                                   }
                            Spacer()
                    }.padding()
                } //VStack
                
                .padding()
                                
                
                            } //ScrollView
            .frame(maxWidth: geometry.size.width * 1)
            }
        .cornerRadius(25)
        }
        
        
    } //GeometryReader



