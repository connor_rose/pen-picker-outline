//
//  Pen+CoreDataProperties.swift
//  Core Data Outline
//
//  Created by Connor Rose on 1/5/21.
//
//

import Foundation
import CoreData


extension Pen {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pen> {
        return NSFetchRequest<Pen>(entityName: "Pen")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String?
    @NSManaged public var nib: String?
    @NSManaged public var notes: String?
    @NSManaged public var pentype: String?
    @NSManaged public var purchaseDate: Date?
    @NSManaged public var brand: Brand?
    @NSManaged public var carries: NSSet?

}

// MARK: Generated accessors for carries
extension Pen {

    @objc(addCarriesObject:)
    @NSManaged public func addToCarries(_ value: Carry)

    @objc(removeCarriesObject:)
    @NSManaged public func removeFromCarries(_ value: Carry)

    @objc(addCarries:)
    @NSManaged public func addToCarries(_ values: NSSet)

    @objc(removeCarries:)
    @NSManaged public func removeFromCarries(_ values: NSSet)

}

extension Pen : Identifiable {

}
