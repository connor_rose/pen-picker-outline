//
//  Carry+CoreDataProperties.swift
//  Core Data Outline
//
//  Created by Connor Rose on 1/5/21.
//
//

import Foundation
import CoreData


extension Carry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Carry> {
        return NSFetchRequest<Carry>(entityName: "Carry")
    }

    @NSManaged public var clean: Bool
    @NSManaged public var dirty: Bool
    @NSManaged public var edcDate: Date?
    @NSManaged public var id: UUID?
    @NSManaged public var notes: String?
    @NSManaged public var currentink: Ink?
    @NSManaged public var currentpen: Pen?

}

extension Carry : Identifiable {

}
