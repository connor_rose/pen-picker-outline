//
//  ContentView.swift
//  Core Data Outline
//
//  Created by Connor Rose on 12/8/20.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Item.timestamp, ascending: true)],
        animation: .default)
    private var items: FetchedResults<Item>

    @FetchRequest(entity: Pen.entity(), sortDescriptors: []) var pens: FetchedResults<Pen>
    @FetchRequest(entity: Carry.entity(), sortDescriptors: []) var carries: FetchedResults<Carry>
//    @ObservedObject var carry: Carry
//    @ObservedObject var pen: Pen
   
    //private var pen: FetchedResults<Pen>
 //   private var carry: FetchedResults<Carry>
    
    var body: some View {
        TabView () {
            EDCListView() .tabItem({
                Image(systemName: "chart.bar.fill")
                Text("EDC")
            }).tag(1)
            
            StatsView()
                .tabItem({
                    Image(systemName: "chart.bar.fill")
                    Text("History")
                }).tag(2)
        }
    }
//        List {
//            ForEach(items) { item in
//                Text("Item at \(item.timestamp!, formatter: itemFormatter)")
//            }
//            .onDelete(perform: deleteItems)
//        }
//        .toolbar {
//            #if os(iOS)
//            EditButton()
//            #endif
//
//            Button(action: addItem) {
//                Label("Add Item", systemImage: "plus")
//            }
//        }
//    }
    
//    func relationshipDemo() {
//        // Create a brand
//        let brand = Brand(context: viewContext)
//        brand.name = "ABC Brand"
//
//        // Create pen
//        let pen = Pen(context: viewContext)
//        pen.name = "ABC Pen"
//        pen.brand = brand
//
//
//        // Save context
//        try! viewContext.save()
//    }
//
    private func addItem() {
        withAnimation {
            let newItem = Item(context: viewContext)
            newItem.timestamp = Date()

            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }

    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { items[$0] }.forEach(viewContext.delete)

            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

