//
//  Core_Data_OutlineApp.swift
//  Core Data Outline
//
//  Created by Connor Rose on 12/8/20.
//

import SwiftUI

@main
struct Core_Data_OutlineApp: App {
    let persistenceController = PersistenceController.shared
   
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
