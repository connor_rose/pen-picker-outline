//
//  StatsView.swift
//  Core Data Outline
//
//  Created by Connor Rose on 12/18/20.
//

import SwiftUI

struct StatsView: View {
    
    
    var body: some View {
        
        // Two buttons to add pen and ink
        VStack {
        addPen()
        }
    }
}

struct addPen: View {
    @Environment(\.managedObjectContext) var viewContext
    
    @FetchRequest(entity: Pen.entity(), sortDescriptors: []) var pens: FetchedResults<Pen>
    @FetchRequest(entity: Brand.entity(), sortDescriptors: []) var brands: FetchedResults<Pen>
    
    @State private var name = ""
  @State private var brand = ""
    @State private var nib = ""
    @State private var pentype = ""
    @State private var notes = "Notes"
    @State private var purchaseDate = Date()
    
   
    
    var dateFormatter: DateFormatter {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            return formatter
        }
    
    var body: some View {
        Form {
            Section {
                TextField("Pen Brand", text: $brand)
                TextField("Pen Name", text: $name)
                TextField("Nib Type", text: $nib)
                TextField("Pen Type", text: $pentype)
                DatePicker("Acquired on", selection: $purchaseDate, displayedComponents: .date)
            }
    }
        Button("Save") {
            let newPen = Pen(context: viewContext)
     //     let newBrand = Brand(context: viewContext)
           
            newPen.name = self.name
            newPen.brand = Brand(context: viewContext)
            newPen.brand?.name = self.brand
            newPen.nib = self.nib
            newPen.pentype = self.pentype
            newPen.notes = self.notes
            newPen.purchaseDate = self.purchaseDate
            newPen.id = UUID()
            try? viewContext.save()
           
        }
}
struct StatsView_Previews: PreviewProvider {
    static var previews: some View {
        StatsView()
    }
}
}
